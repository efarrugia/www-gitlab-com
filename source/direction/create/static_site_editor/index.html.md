---
layout: markdown_page
title: "Category Direction - Static Site Editor"
---

- TOC
{:toc}

## Static Site Editor

| Stage | Maturity |
| --- | --- |
| Create | Minimal |

## Overview

<!--
A good description of what your category is.
If there are special considerations for your strategy or how you plan to prioritize, the description is a great place to include it.
Please include usecases, personas, and user journeys into this section.
-->

Editing static sites should be easy to do and shouldn't require overly technical skills. The Static Site Editor category will
be focused on enhancing the editing experience for static sites inside of GitLab. Because static sites are often used for blogs
or content sites, the SSE team will also be focused on delivering a great content editing experience on mobile and tablets.

### Target Audience

<!--
An overview of the personas involved in this category.
An overview of the evolving user journeys as the category progresses through minimal, viable, complete and lovable maturity levels.
-->

**GitLab Team Members:** As the GitLab team grows, it will be important that every team member can contribute to our handbook, which is a
static site. Therefore, the features delivered out of this group will serve all GitLab team members, both technical and non-technical.

**Leadership:** As companies move to a more distributed way of working, processes and documentation becomes more important. The features
delivered out of this group will also be packaged in a way that will help leadership at other organizations create and manage static sites.

## Where we are Headed

<!--
Describe the future state for your category. 
- What problems are we intending to solve? 
- How will GitLab uniquely address them?
- What is the resulting benefits and value to users and their organizations?

Use narrative techniques to paint a picture of how the lives of your users will benefit from using this category once your strategy is at least minimally realized.
-->

The static site editor experience should be more "what you see is what you get" based, allowing users to make changes easily without deeply understanding git or markdown.

- [Prose Review: merge request discussions on rendered prose](https://gitlab.com/groups/gitlab-org/-/epics/1979)

### What's Next & Why

<!--
This is almost always sourced from the following sections, which describe top priorities for a few stakeholders.
This section must provide a link to an issue or [epic](/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in the category.
-->

- **In Discussion:** [Prose Review: merge request discussions on rendered prose](https://gitlab.com/groups/gitlab-org/-/epics/1979)
    
### What is Not Planned Right Now

<!--
Often it's just as important to talk about what you're not doing as it is to discuss what you are.
This section should include items that people might hope or think we are working on as part of the category, but aren't, and it should help them understand why that's the case.
Also, thinking through these items can often help you catch something that you should in fact do.
We should limit this to a few items that are at a high enough level so someone with not a lot of detailed information about the product can understand the reasoning.
-->

TBD

### Maturity Plan

<!--
It's important your users know where you're headed next.
The maturity plan section captures this by showing what's required to achieve the next level.
-->

Currently, the category is marked as minimal as the GitLab Handbook is comprised of the existing tooling and capabilities, but a much better editor experience
is required for this category to become viable, complete, and lovable.

## Competitive Landscape

[Netlify CMS](https://www.netlifycms.org/) and [Forestry.io](https://forestry.io/) are both products that directionally inspire our vision for
static site editing.

## Business Opportunity

<!--
This section should highlight the business opportunity highlighted by the particular category.
-->

TBD

## Analyst Landscape

<!--
What are analysts and/or thought leaders in the space talking about?
What are one or two issues that will help us stay relevant from their perspective?
-->

TBD

## Top Customer Success/Sales issue(s)

<!--
These can be sourced from the CS/Sales top issue labels when available,
internal surveys, or from your conversations with them.
-->

TBD

## Top user issue(s)

<!--
This is probably the top popular issue from the category (i.e. the one with the most thumbs-up),
but you may have a different item coming out of customer calls.
-->

TBD

## Top internal customer issue(s)

<!--
These are sourced from internal customers wanting to [dogfood](/handbook/product/#dogfood-everything) the product.
-->

TBD

## Top Vision Item(s)

<!--
What's the most important thing to move your vision forward?
-->

TBD
